import globalPluginHandler
import api
import wx
import gui
import config
import controlTypes
from NVDAObjects.IAccessible import WindowRoot

class NavigationTree(wx.TreeCtrl):

	def __init__(self, *args, **kwargs):
		super(NavigationTree, self).__init__(*args, **kwargs)
		self.Bind(wx.EVT_TREE_ITEM_EXPANDING, self.onExpandItem)
		self.Bind(wx.EVT_TREE_ITEM_COLLAPSING, self.onCollapseItem)
		self._collapsing = False
		self.root = self.AddRoot("")

	def setRoot(self, root):
		if root is not None and root.name is not None and root.name != "":
			self.SetItemText(self.root, root.name)
		self.SetItemHasChildren(self.root)
		self.SetPyData(self.root, root)

	def onExpandItem(self, event):
		treeItem=event.GetItem()
		for child in self.GetPyData(treeItem).children:
			if child is not None and (child.name is not None or len(child.children)>0):
				if controlTypes.STATE_INVISIBLE not in child._get_states():
					treeSubItem=self.AppendItem(treeItem, self.formatLabel(child))
					self.SetItemHasChildren(treeSubItem, len(child.children)>0)
					self.SetPyData(treeSubItem, child)

	def onCollapseItem(self, event):
		# Be prepared, self.CollapseAndReset below may cause
		# another wx.EVT_TREE_ITEM_COLLAPSING event being triggered.
		if self._collapsing:
			event.Veto()
		else:
			self._collapsing = True
			item = event.GetItem()
			self.CollapseAndReset(item)
			self.SetItemHasChildren(item)
			self._collapsing = False

	def formatLabel(self, obj):
		if obj is not None:
			label=unicode(obj.name)
			role=obj.role
			for name, const in controlTypes.__dict__.iteritems():
					if name.startswith("ROLE_") and role == const:
						role = name
						break
			label+=" ("+role[5:]+")"
			return label
		return u'None'

class NavigationTreeFrame(wx.Frame):

	def __init__(self, *args, **kwargs):
		super(NavigationTreeFrame, self).__init__(*args, **kwargs)
		self._tree = NavigationTree(self)

	def setRoot(self, root):
		self._tree.setRoot(root)

class GlobalPlugin(globalPluginHandler.GlobalPlugin):

	def script_showTree(self, gesture):
		fgObj=api.getForegroundObject()
		if isinstance(fgObj.parent, WindowRoot):
			fgObj=fgObj.parent
		def run():
			gui.mainFrame.prePopup()
			d = NavigationTreeFrame(None)
			d.setRoot(fgObj)
			d.Show()
			gui.mainFrame.postPopup()
		wx.CallAfter(run)
	# Translators: the description for the objects list dialog script.
	script_showTree.__doc__ = _("Presents a tree of object")

	__gestures={
		"kb:NVDA+Shift+Y": "showTree",
	}